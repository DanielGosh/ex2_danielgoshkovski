#include "Gene.h"
/*
init function for the gene.
input: int start, int end, bool on_complementary_dna_stran
returns: none
*/
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_strand_dna_complementary_on = on_complementary_dna_strand;
}
/*
get function for the start value.
input: none
returns: start value of the gene
*/
unsigned int Gene::GetStart() const
{
	return this->_start;
}
/*
get function for the end value.
input: none
returns: end value of the gene
*/
unsigned int Gene::GetEnd() const
{
	return this->_end;
}
/*
get function for the _strand_dna_complementary_on value.
input: none
returns: _strand_dna_complementary_on value of the gene
*/
bool Gene::is_on_complementary_dna_strand() const
{
	return this->_strand_dna_complementary_on;
}
/*
set function for the start value.
input: start value of the gene
returns: none
*/
void Gene::SetStart(unsigned int start)
{
	this->_start = start;
}
/*
set function for theend value.
input: end value of the gene
returns: none
*/
void Gene::SetEnd(unsigned int end)
{
	this->_end = end;
}
/*
set function for the _strand_dna_complementary_on value.
input: _strand_dna_complementary_on value of the gene
returns: none
*/
void Gene::Set_complementary_dna_strand(bool on_complementary_dna_strand)
{
	this->_strand_dna_complementary_on = on_complementary_dna_strand;
}