#pragma once
#include <string>
#include <iostream>
#include "Gene.h"
class Nucleus
{
private:
	std::string _DNA_strand;
	std::string _complementary_DNA_strand;
public:
	void init(const std::string dna_sequence);
	std::string GetComplementary();
	std::string get_RNA_transcript(const Gene& gene) const;
	std::string get_reversed_DNA_strand() const;
};

