#include "Mitochondrion.h"
/*
initiation function for the mitocondrion
input: none
return: none
*/
void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}
/*
function to set glucose level
input: glocuse_units
ouput: none
*/
void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}
/*
function to check if atp production is possible.
input: none
return: True or False depending if it is possible to produce atp or not
*/
bool Mitochondrion::produceATP() const
{
	if (this->_has_glocuse_receptor && this->_glocuse_level >= 50)
	{
		return true;
	}
	return false;
}
/*
function that checks the inserted protein to see if it creates a glucose reseptor.
if so it changes the value of the glucose receptor status
input: protein reference
return: none
*/
void Mitochondrion::insert_glucose_receptor(const Protein& protein)
{
	std::string blank;
	AminoAcidNode* curr = protein.get_first();
	while (curr)
	{
		blank += curr->get_data();
		blank += ",";
	}
	if (blank == "ALANINE,LEUCINE,GLYCINE,HISTIDINE,LEUCINE,PHENYLALANINE")
	{
		this->_has_glocuse_receptor = true;
	}
}