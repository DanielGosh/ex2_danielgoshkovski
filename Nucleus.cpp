#include "Nucleus.h"
#include <string>
#include <iostream>
/*
initiation function for the nucleus.
input: string of the dna sequence.
return: none.
*/
void Nucleus::init(const std::string dna_sequence)
{
	int i = 0;
	this->_DNA_strand = dna_sequence;
	for (i = 0; i < this->_DNA_strand.length(); i++)
	{
		if (this->_DNA_strand[i] == 'G')
		{
			this->_complementary_DNA_strand.append("C");
		}
		else if (this->_DNA_strand[i] == 'C')
		{
			this->_complementary_DNA_strand.append("G");
		}
		else if (this->_DNA_strand[i] == 'T')
		{
			this->_complementary_DNA_strand.append("A");
		}
		else if (this->_DNA_strand[i] == 'A')
		{
			this->_complementary_DNA_strand.append("T");
		}
	}
}
/*
getter function for the dna coplementary.
input: none.
output: string of the coplementary dna
*/
std::string Nucleus::GetComplementary()
{
	return this->_complementary_DNA_strand;
}
/*
function that creates and returns the rna transcript.
the function uses the gene data to choose the correct dna strand and creates the rna from that.
input: gene reference
return: string of the rna
*/
std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	std::string blank;
	std::string strand;
	int i = gene.GetStart();
	int end = gene.GetEnd();
	if (gene.is_on_complementary_dna_strand())
	{
		strand = this->_complementary_DNA_strand;
	}
	else
	{
		strand = this->_DNA_strand;
	}
	for (i = gene.GetStart(); i<= end;i++)
	{
		blank += strand[i];
	}
	for (i = 0;i < blank.length();i++)
	{
		if (blank[i] == 'T')
		{
			blank[i] = 'U';
		}
	}
	return blank;

}
/*
function that returns the reversed string of the dna strand.
input: none.
output: the reversed dna strand.
*/
std::string Nucleus::get_reversed_DNA_strand() const
{
	std::string strand = this->_DNA_strand;
	int n = strand.length();
	for (int i = 0; i < n/2; i++)
	{
		std::swap(strand[i], strand[n-i-1]);
	}
	return strand;
}