#pragma once
class Gene
{
private:
	unsigned int _start;
	unsigned int _end;
	bool _strand_dna_complementary_on;

public:
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);
	unsigned int GetStart() const;
	unsigned int GetEnd() const;
	bool is_on_complementary_dna_strand() const;
	void SetStart(unsigned int start);
	void SetEnd(unsigned int end);
	void Set_complementary_dna_strand(bool on_complementary_dna_strand);
};

